# Chang Sound Systems

## Server

* Go to root directory
* Run `docker-compose up -d postgres`
* Open server folder in IntelliJ IDEA
* Open Server.java file
* Right click main method and run

## Web

* Go to web directory
* Run `npm install`
* Run `ng serve` to launch the web app

## Web Credentials

* Username: `admin`
* Password: `admin@123`
