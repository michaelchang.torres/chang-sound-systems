import { ItemsApi } from '../items/items-api';
import { Apollo } from 'apollo-angular';

export class ApiClient {
  private readonly itemsApi;

  constructor(private apollo: Apollo) {
    this.itemsApi = new ItemsApi(apollo);
  }

  items(): ItemsApi {
    return this.itemsApi;
  }
}
