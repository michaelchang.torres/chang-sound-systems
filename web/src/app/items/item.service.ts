import { Injectable } from '@angular/core';
import { Item, ItemModel } from './item';
import { ReservationModel } from '../reservations/reservation';

@Injectable()
export class ItemService {
  constructor() { }

  create(item: ItemModel) {
    return Item.create(item);
  }

  list() {
    return Item.list();
  }

  get(id: string) {
    return Item.get(id);
  }

  reserve(item: ItemModel, reservation: ReservationModel) {
    return Item.reserve(item, reservation);
  }

  unreserve(item: ItemModel) {
    return Item.unreserve(item);
  }
}
