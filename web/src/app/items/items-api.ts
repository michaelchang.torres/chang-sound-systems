import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { ItemModel } from './item';
import { ReservationModel } from '../reservations/reservation';

export class ItemsApi {
  constructor(private apollo: Apollo) {
  }

  create(
    item: ItemModel
  ) {
    const mutation = gql`
      mutation createItem(
        $name: String!
        $purchaseDate: String!
      ) {
        createItem(
          name: $name,
          purchaseDate: $purchaseDate,
        ) {
          id
        }
      }
    `;

    return this.apollo
      .mutate({
        variables: {
          name: item.name,
          purchaseDate: item.purchaseDate,
        },
        mutation
      }).toPromise();
  }

  get(id: string) {
    const query = gql`
      query getItem (
        $id: ID!
      ) {
          getItem(
            id: $id
          ) {
          id
          created
          updated
          name
          status
          purchaseDate
          reservations {
            ... on Reservation {
              reservee
              startDate
              endDate
            }
          }
        }
      }
    `;

    return this.apollo
      .query({
        variables: {
          id
        },
        query
      }).toPromise();
  }

  list() {
    const query = gql`
      query listItems(
        $max: Int,
        $offset: Int,
        $sort: String
      ) {
        listItems(
          opts: {
            max: $max,
            offset: $offset,
            sort: $sort
          }
        ) {
          total
          items {
            ... on Item {
              id
              created
              updated
              name
              purchaseDate
              status
              reservations {
                ... on Reservation {
                  reservee
                  startDate
                  endDate
                }
              }
            }
          }
        }
      }
    `;

    return this.apollo
      .query({
        variables: {
          offset: 0,
          sort: 'name:asc',
        },
        query
      }).toPromise();
  }

  reserve(
    item: ItemModel,
    reservation: ReservationModel
  ) {
    const mutation = gql`
      mutation reserveItem(
        $id: ID!
        $reservee: String!
        $startDate: String!
        $endDate: String!
      ) {
        reserveItem(
          id: $id,
          reservee: $reservee,
          startDate: $startDate,
          endDate: $endDate
        ) {
          id
        }
      }
    `;

    return this.apollo
      .mutate({
        variables: {
          id: item.id,
          reservee: reservation.reservee,
          startDate: reservation.startDate,
          endDate: reservation.endDate
        },
        mutation
      }).toPromise();
  }

  unreserve(
    item: ItemModel
  ) {
    const mutation = gql`
      mutation unreserveItem(
        $id: ID!
      ) {
        unreserveItem(
          id: $id,
        ) {
          id
        }
      }
    `;

    return this.apollo
      .mutate({
        variables: {
          id: item.id
        },
        mutation
      }).toPromise();
  }
}
