import { Model } from '../model';
import { ReservationModel } from '../reservations/reservation';
import { Status } from './status';

export class Item extends Model {
  static create(item: ItemModel) {
    return this.api().items().create(item);
  }

  static list() {
    return this.api().items().list();
  }

  static get(id: string) {
    return this.api().items().get(id);
  }

  static reserve(item: ItemModel, reservation: ReservationModel) {
    return this.api().items().reserve(item, reservation);
  }

  static unreserve(item: ItemModel) {
    return this.api().items().unreserve(item);
  }
}

export interface ItemModel {
  id: string;
  name: string;
  purchaseDate: string;
  status: Status;
  reservations: ReservationModel[];
}
