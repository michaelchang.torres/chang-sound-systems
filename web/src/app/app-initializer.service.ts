import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { ApiClient } from './api/api-client';
import { Model } from './model';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AppInitializerService {
  constructor(
    private apollo: Apollo,
    private http: HttpClient
  ) { }

  init() {
    Model.setApiClient(new ApiClient(this.apollo));
  }
}
