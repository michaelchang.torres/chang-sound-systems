import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER, Injector } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { ItemsCreateComponent } from './pages/items/items-create/items-create.component';
import { ItemsViewComponent } from './pages/items/items-view/items-view.component';
import { ItemsIndexComponent } from './pages/items/items-index/items-index.component';
import { LoginComponent } from './pages/users/login/login.component';
import { ItemService } from './items/item.service';
import { ApolloModule } from 'apollo-angular';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { AppInitializerService } from './app-initializer.service';
import { HttpLink, HttpLinkModule } from 'apollo-angular-link-http';
import { Apollo } from 'apollo-angular';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { ItemsReserveComponent } from './pages/reservations/items-reserve/items-reserve.component';
import { BaseComponent } from './pages/base/base/base.component';
import { FormsModule } from '@angular/forms';

const appRoutes: Routes = [
  {
    path: 'items/create',
    component: ItemsCreateComponent,
    data: { title: 'Items Create' }
  },
  {
    path: 'items/index',
    component: ItemsIndexComponent,
    data: { title: 'Items Index' }
  },
  {
    path: 'items/reserve/:id',
    component: ItemsReserveComponent,
    data: { title: 'Items Reserve' }
  },
  {
    path: 'items/view/:id',
    component: ItemsViewComponent,
    data: { title: 'Items View' }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: { title: 'Login' }
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    ItemsCreateComponent,
    ItemsViewComponent,
    ItemsIndexComponent,
    LoginComponent,
    ItemsReserveComponent,
    BaseComponent
  ],
  imports: [
    ApolloModule,
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    HttpClientModule,
    HttpLinkModule
  ],
  exports: [ RouterModule ],
  providers: [
    ItemService,
    {
      provide: APP_INITIALIZER,
      useFactory: () => function () {
        const ais = new AppInitializerService(
          AppModule.injector.get(Apollo),
          AppModule.injector.get(HttpClient)
        );
        ais.init();
        return ais;
      },
      deps: [HttpClient],
      multi: true
    },
    Apollo,
    AppInitializerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  static injector: Injector;
  config: any;

  constructor(
    injector: Injector,
    apollo: Apollo,
    httpLink: HttpLink,
    http: HttpClient
  ) {
    AppModule.injector = injector;

    const link = httpLink.create({
      uri: 'http://localhost:8080/server'
    });

    apollo.create({
      link,
      cache: new InMemoryCache(),
      defaultOptions: {
        watchQuery: {
          fetchPolicy: 'no-cache',
          errorPolicy: 'ignore',
        },
        query: {
          fetchPolicy: 'no-cache',
          errorPolicy: 'all',
        },
      }
    });
  }
}
