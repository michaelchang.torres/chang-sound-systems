import { Component, OnInit } from '@angular/core';
import { ItemService } from '../../../items/item.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ReservationModel } from '../../../reservations/reservation';
import { ItemModel } from '../../../items/item';
import { BaseComponent } from '../../base/base/base.component';

@Component({
  selector: 'app-items-reserve',
  templateUrl: './items-reserve.component.html',
  styleUrls: ['./items-reserve.component.css']
})
export class ItemsReserveComponent extends BaseComponent implements OnInit {
  public reservation: ReservationModel = {
    reservee: '',
    startDate: '',
    endDate: ''
  } as any;

  constructor(
    private itemService: ItemService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    super();
  }

  ngOnInit() {
  }

  reserve() {
    this.activatedRoute.params.subscribe(params => {
      this.itemService.reserve({ id: params.id } as ItemModel, this.reservation).then(
        (data: any) => {
          this.router.navigateByUrl(`/items/view/${params.id}`);
          alert('Successfully reserved an item!');
        },
        error => {
          alert('An error has occurred');
        }
      );
    });
  }

  routeView() {
    this.activatedRoute.params.subscribe(params => {
      this.router.navigateByUrl(`/items/view/${params.id}`);
    });
  }
}
