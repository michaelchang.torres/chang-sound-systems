import { Component, OnInit, ViewChild } from '@angular/core';
import { ItemService } from '../../../items/item.service';
import { Router } from '@angular/router';
import { ItemModel } from '../../../items/item';
import { BaseComponent } from '../../base/base/base.component';

@Component({
  selector: 'app-items-create',
  templateUrl: './items-create.component.html',
  styleUrls: ['./items-create.component.css']
})
export class ItemsCreateComponent extends BaseComponent implements OnInit {
  public item: ItemModel = {
    id: '',
    name: '',
    purchaseDate: '',
    status: '',
    reservations: []
  } as any;

  constructor(
    private itemService: ItemService,
    private router: Router
  ) {
    super();
  }

  ngOnInit() {
  }

  create() {
    this.itemService.create(this.item).then((data: any) => {
      this.router.navigateByUrl('/items/index');
      alert('Successfully created an item!');
    }, error => {
      alert('An error has occurred');
    });
  }
}
