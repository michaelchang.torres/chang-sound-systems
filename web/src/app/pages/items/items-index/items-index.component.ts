import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ItemService } from '../../../items/item.service';
import { Router } from '@angular/router';
import { Status } from '../../../items/status';
import { $enum } from 'ts-enum-util';
import { BaseComponent } from '../../base/base/base.component';

@Component({
  selector: 'app-items-index',
  templateUrl: './items-index.component.html',
  styleUrls: ['./items-index.component.css']
})
export class ItemsIndexComponent extends BaseComponent implements OnInit, AfterViewInit {
  public items: any;

  constructor(
    private itemService: ItemService,
    private router: Router
  ) {
    super();
  }

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.updateTable();
  }

  updateTable() {
    this.itemService.list().then(
      (data: any) => {
        this.items = data.data.listItems.items;
      },
      error => {
        alert('An error has occurred');
      }
    );
  }

  view(id) {
    this.router.navigateByUrl(`/items/view/${id}`);
  }

  formatStatus(status) {
    return $enum(Status).getValueOrDefault(status);
  }
}
