import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ItemService } from '../../../items/item.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ItemModel } from '../../../items/item';
import { $enum } from 'ts-enum-util';
import { Status } from '../../../items/status';
import { BaseComponent } from '../../base/base/base.component';

@Component({
  selector: 'app-items-view',
  templateUrl: './items-view.component.html',
  styleUrls: ['./items-view.component.css']
})
export class ItemsViewComponent extends BaseComponent implements OnInit, AfterViewInit {
  public item: ItemModel = {
    id: '',
    name: '',
    purchaseDate: '',
    status: '',
    reservations: []
  } as any;

  constructor(
    private itemService: ItemService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    super();
  }

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.get();
  }

  get() {
    this.activatedRoute.params.subscribe(params => {
      this.itemService.get(params.id).then(
        (data: any) => {
          this.item = data.data.getItem;
          this.item.status = $enum(Status).getValueOrDefault(this.item.status);
        },
        error => {
        }
      );
    });
  }

  routeReserve() {
    this.router.navigateByUrl(`/items/reserve/${this.item.id}`);
  }

  unreserve() {
    this.itemService.unreserve({ id: this.item.id } as ItemModel).then(
      (data: any) => {
        this.get();
      },
      error => {
        alert('An error has occurred');
      }
    );
  }
}
