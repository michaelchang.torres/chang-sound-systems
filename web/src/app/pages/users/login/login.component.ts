import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { ItemService } from '../../../items/item.service';
import { BaseComponent } from '../../base/base/base.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent extends BaseComponent implements OnInit {
  @ViewChild('generalError') generalError: ElementRef;

  public items: any[];
  public credentials: any = {
    username: '',
    password: ''
  };

  constructor(
    private itemService: ItemService,
    private router: Router
  ) {
    super();
  }

  ngOnInit() {

  }

  login() {
    if (this.credentials.username === 'admin' && this.credentials.password === 'admin@123') {
      this.router.navigateByUrl('/items/index');

      this.itemService.list().then(
        (data: any) => {
          this.items = data.data.listItems.items;
          const reserved = this.items.filter(e => e.status === 'RESERVED').length;

          if (reserved !== 0) {
            alert(`You currently have ${reserved} item/s reserved. Please be mindful of their end dates.`);
          }
        },
        error => {
          alert('An error has occurred');
        }
      );
    } else {
      alert('Invalid Username/Password');
    }
  }
}
