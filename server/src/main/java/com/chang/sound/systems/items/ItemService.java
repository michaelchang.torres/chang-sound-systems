package com.chang.sound.systems.items;

import com.chang.sound.systems.*;
import com.chang.sound.systems.model.*;

import java.util.*;
import java.util.function.*;

@org.springframework.stereotype.Service
public class ItemService {

  public Item create(Item item) {
    return (Item)transaction(item::save);
  }

  public Item update(Item item) {
    return (Item)transaction(item::save);
  }

  public PagedList<Item> list(ListOpts opts) {
    return Item.list(opts);
  }

  public Optional<Item> get(String id) {
    return Item.get(id);
  }

  public <T> T transaction(Supplier<T> closure) {
    return JPAUtil.transaction(closure::get);
  }

  public void transaction(Runnable closure) {
    JPAUtil.transaction(closure);
  }
}


