package com.chang.sound.systems.model;

import java.util.*;

public class PagedList<T extends Model> extends ArrayList<T> {
  private final long total;
  private Collection<T> items;
  private final Class<T> type;

  public PagedList(Collection<T> seed, long total, Class<T> type) {
    super(seed);
    items = seed;
    this.total = total;
    this.type = type;
  }

  public long getTotal() {
    return total;
  }

  @Override
  public boolean equals(Object obj) {
    return super.equals(obj);
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }

  public Class<T> getType() {
    return this.type;
  }

  public Collection<T> getItems() {
    return this.items;
  }
}
