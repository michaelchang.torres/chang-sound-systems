package com.chang.sound.systems;

import org.slf4j.*;

import javax.persistence.*;
import java.util.*;
import java.util.function.*;

public class JPAUtil {
  private static final Logger log = LoggerFactory.getLogger(JPAUtil.class);
  private static ThreadLocal<EntityManager> threadLocal = new ThreadLocal<>();
  private static EntityManagerFactory emFactory;

  private JPAUtil() {}

  public static Properties mkProperties(Config config) {
    Properties props = new Properties();
    props.setProperty("hibernate.connection.url", config.getProperty("db.url"));
    props.setProperty("hibernate.connection.username", config.getProperty("db.username"));
    props.setProperty("hibernate.connection.password", config.getProperty("db.password"));

    props.setProperty("hibernate.dialect", config.getProperty("db.dialect"));
    props.setProperty("hibernate.hbm2ddl.auto", config.getProperty("db.ddl"));
    props.setProperty("hibernate.show_sql", config.getProperty("db.showSql"));
    props.setProperty("hibernate.connection.driver_class", "org.postgresql.Driver");

    return props;
  }

  public static EntityManager getEntityManager() {
    return Optional.ofNullable(threadLocal.get())
               .orElseGet(() -> {
                 final EntityManager em = emFactory.createEntityManager();
                 threadLocal.set(em);
                 return em;
               });
  }

  public static void init(Config config) {
    Thread.currentThread().setContextClassLoader(JPAUtil.class.getClassLoader());
    emFactory = Persistence.createEntityManagerFactory("inventory_system", mkProperties(config));
  }

  public static void transaction(Runnable closure) {
    transaction(() -> {
      closure.run();
      return null;
    });
  }

  public static <T> T transaction(Supplier<T> closure) {
    final EntityManager em = getEntityManager();
    final EntityTransaction trx = em.getTransaction();
    boolean owner = false;

    if(!trx.isActive()) {
      trx.begin();
      owner = true;
    }

    try {
      T result = closure.get();

      if(owner && trx.isActive()) {
        trx.commit();
        em.close();
        threadLocal.remove();
      }

      return result;
    }
    catch(Exception ex) {
      try {
        if(trx.isActive())
          trx.rollback();
      }
      catch(Exception e) {
        log.error("Exception", e);
      }

      throw ex;
    }
  }

  public static void shutdown() {
    if(emFactory.isOpen()) {
      try {
        emFactory.close();
      }
      catch(IllegalStateException ex) {
        log.error("IllegalStateException", ex);
      }
    }
  }
}