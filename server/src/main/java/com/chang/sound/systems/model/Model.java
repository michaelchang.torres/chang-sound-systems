package com.chang.sound.systems.model;

import com.chang.sound.systems.*;
import org.hibernate.*;

import javax.persistence.*;
import java.time.*;
import java.util.*;

import static com.chang.sound.systems.JPAUtil.*;

@MappedSuperclass
public abstract class Model {
  public static final String DATE_FORMATTER= "yyyy-MM-dd HH:mm:ss.SSS";

  @Id
  protected String id = UUID.randomUUID().toString();

  @Column(nullable = false)
  private LocalDateTime created;

  @Column(nullable = false)
  private LocalDateTime updated;

  public Model save() {
    getEntityManager()
        .unwrap(Session.class)
        .saveOrUpdate(this);
    return this;
  }

  public String getId() {
    return id;
  }

  public String getCreated() {
    return Util.format(created);
  }

  public String getUpdated() {
    return Util.format(updated);
  }

  @PrePersist
  public void beforeInsert() {
    created = OffsetDateTime.now(ZoneId.of("UTC")).toLocalDateTime();
    updated = OffsetDateTime.now(ZoneId.of("UTC")).toLocalDateTime();
  }

}