package com.chang.sound.systems;

import com.chang.sound.systems.model.*;

import java.time.*;
import java.time.format.*;

public class Util {
  public static String format(LocalDateTime date) {
    return date.format(DateTimeFormatter.ofPattern(Model.DATE_FORMATTER));
  }
}
