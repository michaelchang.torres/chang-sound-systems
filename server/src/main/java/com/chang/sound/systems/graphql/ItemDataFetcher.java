package com.chang.sound.systems.graphql;

import com.chang.sound.systems.items.*;
import com.chang.sound.systems.model.*;
import com.chang.sound.systems.reservations.*;
import graphql.schema.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import java.time.*;
import java.time.format.*;
import java.util.*;

@Component
public class ItemDataFetcher {
  @Autowired
  private ItemService itemService;

  public DataFetcher create() {
    return dataFetchingEnvironment -> {

      try {
        Map<String, Object> params = dataFetchingEnvironment.getArguments();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Model.DATE_FORMATTER);

        return itemService.create(
          new Item(
            params.get("name").toString(),
            LocalDateTime.parse(params.get("purchaseDate").toString(), formatter)
          )
        );
      }
      catch(Exception ex) {
        throw ex;
      }
    };
  }

  public DataFetcher list() {
    return dataFetchingEnvironment ->
      itemService.list(ListOpts.from(dataFetchingEnvironment.getArgument("opts")));
  }

  public DataFetcher get() {
    return dataFetchingEnvironment ->
      itemService.get(dataFetchingEnvironment.getArgument("id")).orElse(null);
  }

  public DataFetcher reserve() {
    return dataFetchingEnvironment -> {
      Map<String, Object> params = dataFetchingEnvironment.getArguments();
      DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Model.DATE_FORMATTER);

      return itemService.get(dataFetchingEnvironment.getArgument("id")).map(item -> {
        item.addReservation(
          new Reservation(
            params.get("reservee").toString(),
            LocalDateTime.parse(params.get("startDate").toString(), formatter),
            LocalDateTime.parse(params.get("endDate").toString(), formatter),
            item
          )
        );

        return itemService.update(item.reserve());
      }).orElse(null);
    };
  }

  public DataFetcher unreserve() {
    return dataFetchingEnvironment -> {
      Map<String, Object> params = dataFetchingEnvironment.getArguments();

      return itemService.get(dataFetchingEnvironment.getArgument("id"))
        .map(item -> itemService.update(item.unreserve())).orElse(null);
    };
  }
}
