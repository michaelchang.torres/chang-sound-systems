package com.chang.sound.systems.items;

import com.chang.sound.systems.*;
import com.chang.sound.systems.model.*;
import com.chang.sound.systems.reservations.*;

import javax.persistence.*;
import java.time.*;
import java.util.*;

@Entity
public class Item extends Model {
  private static final Repository<Item> repo = new Repository(Item.class);

  @Column
  private String name;

  @Column
  private LocalDateTime purchaseDate;

  @Column
  @Enumerated(EnumType.STRING)
  Status status = Status.IN_STORAGE;

  @OneToMany(fetch = FetchType.EAGER, mappedBy = "item", cascade = CascadeType.ALL, orphanRemoval = true)
  List<Reservation> reservations = new ArrayList<Reservation>();

  public Item(String name, LocalDateTime purchaseDate) {
    setName(name);
    setPurchaseDate(purchaseDate);
  }

  public Item() {

  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPurchaseDate() {
    return Util.format(purchaseDate);
  }

  public void setPurchaseDate(LocalDateTime purchaseDate) {
    this.purchaseDate = purchaseDate;
  }

  public static PagedList<Item> list(ListOpts opts) {
    return repo.list(opts);
  }

  public static Optional<Item> get(String id) {
    return repo.get(id);
  }

  public void addReservation(Reservation arg) {
    this.reservations.add(arg);
  }

  public Status getStatus() {
    return status;
  }

  public Item reserve() {
    this.status = Status.RESERVED;
    return this;
  }

  public Item unreserve() {
    this.status = Status.IN_STORAGE;
    return this;
  }

  public void setStatus(Status status) {
    this.status = status;
  }
}
