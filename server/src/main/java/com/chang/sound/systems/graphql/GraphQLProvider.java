package com.chang.sound.systems.graphql;

import com.chang.sound.systems.items.*;
import com.chang.sound.systems.model.*;
import com.google.common.io.Resources;
import graphql.*;
import graphql.schema.*;
import graphql.schema.idl.*;
import org.apache.commons.compress.utils.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.annotation.*;
import org.springframework.stereotype.*;

import javax.annotation.*;
import java.io.*;
import java.net.*;

import static graphql.schema.idl.TypeRuntimeWiring.*;

@Component
public class GraphQLProvider {
  private GraphQL graphQL;

  @Bean
  public GraphQL graphQL() {
    return graphQL;
  }

  @Autowired
  ItemDataFetcher itemDataFetcher;


  @PostConstruct
  public void init() throws IOException {
    URL url = Resources.getResource("schema.graphql");
    String sdl = Resources.toString(url, Charsets.UTF_8);
    GraphQLSchema graphQLSchema = buildSchema(sdl);
    this.graphQL = GraphQL.newGraphQL(graphQLSchema).build();
  }

  private GraphQLSchema buildSchema(String sdl) {
    TypeDefinitionRegistry typeRegistry = new SchemaParser().parse(sdl);
    RuntimeWiring runtimeWiring = buildWiring();
    SchemaGenerator schemaGenerator = new SchemaGenerator();
    return schemaGenerator.makeExecutableSchema(typeRegistry, runtimeWiring);
  }

  private RuntimeWiring buildWiring() {
    return RuntimeWiring.newRuntimeWiring()
      .type(newTypeWiring("Query")
        .dataFetcher("listItems", itemDataFetcher.list())
        .dataFetcher("getItem", itemDataFetcher.get())
      )
      .type(newTypeWiring("Mutation")
        .dataFetcher("createItem", itemDataFetcher.create())
        .dataFetcher("reserveItem", itemDataFetcher.reserve())
        .dataFetcher("unreserveItem", itemDataFetcher.unreserve())
      )
      .type("PagedList", typeWriting -> typeWriting.typeResolver(pagedListResolver))
      .build();
  }

  TypeResolver pagedListResolver = env -> {
    Class t = ((PagedList) env.getObject()).getType();

    if(t == Item.class)
      return env.getSchema().getObjectType("ItemsPagedList");

    else
      return env.getSchema().getObjectType("Model");
  };
}
