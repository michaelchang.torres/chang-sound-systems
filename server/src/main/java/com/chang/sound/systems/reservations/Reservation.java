package com.chang.sound.systems.reservations;

import com.chang.sound.systems.*;
import com.chang.sound.systems.items.*;
import com.chang.sound.systems.model.*;
import org.apache.tomcat.jni.*;

import javax.persistence.*;
import java.time.*;

@Entity
public class Reservation extends Model {
  @Column
  private String reservee;

  @Column
  private LocalDateTime startDate;

  @Column
  private LocalDateTime endDate;

  public Reservation() {}

  public Reservation(String reservee, LocalDateTime startDate, LocalDateTime endDate, Item item) {
    this.reservee = reservee;
    this.startDate = startDate;
    this.endDate = endDate;
    this.item = item;
  }

  @ManyToOne
  private Item item;

  public String getStartDate() {
    return Util.format(startDate);
  }

  public String getEndDate() {
    return Util.format(endDate);
  }
}
